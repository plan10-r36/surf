/* Welcome to Surf! */

/* Path configuration */
static char *scriptfile     = "~/.surf/script.js";
static char *styledir       = "~/.surf/styles/";
static char *cachefolder    = "~/.surf/cache/";
static char *dbfolder       = "~/.surf/databases/";

/* visual options */
static Bool kioskmode       = FALSE; /* Ignore shortcuts */
static Bool showindicators  = TRUE;  /* Show indicators in window title */
static Bool zoomto96dpi     = TRUE;  /* Zoom pages to always emulate 96dpi */
static Bool runinfullscreen = FALSE; /* Run in fullscreen mode by default */
static Bool accelrendering  = TRUE;  /* Allow accelerated compositing of
				      *	the page, using the GPU, if available. */

static char *defaultencoding     = "utf-8"; /* Default encoding for files. */
static guint defaultfontsize     = 12;      /* Default font size */
static guint defaultmonofontsize = 10;   /* Default monospace font size */
static gfloat zoomlevel          = 1.0;     /* Default zoom level */

/* Soup default features */
static char *cookiefile     = "~/.surf/cookies.txt";
static char *cookiepolicies = "Aa@"; /* A: accept all; a: accept nothing,
                                        @: accept all except third party */
static char *cafile         = "/etc/ssl/certs/ca-certificates.crt";
static Bool strictssl       = TRUE; /* strict means if untrusted SSL/TLS
				     * connections should be refused. */
static time_t sessiontime   = 3600;
static Bool enablediskcache = TRUE;
static int diskcachebytes   = 5 * 1024 * 1024;
#define uafile                "~/.surf/useragents.txt"

/* Webkit default features */
static Bool enablescrollbars      = TRUE;
static Bool enablespatialbrowsing = TRUE;
static Bool enablespellchecking   = FALSE;
static Bool enableplugins         = TRUE;
static Bool enablepagecache       = TRUE; /* Enable cache of pages in current
					   * history. This will save memory
					   * if you do not have any. */
static Bool privatebrowsing       = FALSE; /* Set default value for private
					    * browsing. */
static Bool enablescripts         = TRUE;
static Bool enableinspector       = TRUE;
static Bool enablestyle           = TRUE;
static Bool enablehtml5db         = TRUE;
static Bool enablehtml5local      = TRUE;
static Bool enablejava            = FALSE; /* Toggle if <applet> is allowed. */
static Bool enablemediastream     = FALSE; /* Allow access to local video
					    * and audio input devices. */
static Bool enablemediasource     = FALSE; /* Allow JS to generate media
					    * streams. */
static Bool enablewebaudio        = FALSE; /* Allow JS to generate wav
					    * streams. */
static Bool inlineplayback        = TRUE;  /* Toggle if media should be played
					    * inline. */
static Bool inlinegestures        = TRUE;  /* Toggle if media playbeck
					    * requires some click to start. */
static Bool enablewebgl           = TRUE;
static Bool dnsprefetching        = FALSE; /* DNS prefetching is insecure,
					    * so disabled. */
static Bool offlineappcache       = FALSE; /* Allow offline web application
					    * cache? NO! */
static Bool loadimages            = TRUE;
static Bool hidebackground        = FALSE;
static Bool allowgeolocation      = TRUE;
static Bool insecureresources     = FALSE; /* Whether to allow to load
					    * non-HTTPS resources in HTTPS pages. */

/*
 * Now on TV: »What is the best User-Agent for me?«
 * Result: None.
 */
char *useragent = " ";
/*
 * These are for the case some incompetent »web programmer« decided for you
 * what is best for you.
 */
/*
static char *useragent      = "Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) "
        "Gecko/41.0 Firefox/41.0";
static char *useragent      = "Mozilla/5.0 (X11; Linux x86_64) "
        "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.19 "
	"Safari/537.36";
static char *useragent      = "Mozilla/5.0 (X11; U; Unix; en-US) "
	"AppleWebKit/537.15 (KHTML, like Gecko) Chrome/24.0.1295.0 "
	"Safari/537.15 Surf/"VERSION;
*/

/* custom http headers */
static HttpHeader customheaders[] = {
	/* key, value */
	/* Do-Not-Track. Haha! */
	{ "DNT", "1" },
	/* We are a damn US imperialist. Change to cn, once Chinese communism
	 * has won.*/
	{ "Accept-Language", "en-US,en;q=0.5" },
	{ "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" },
	{ "Accept-Encoding", "gzip, deflate" },
	/* We are ready for TLS. Is it still secure? */
	{ "Upgrade-Insecure-Requests", "1" },
};

#define PROMPT_GOTO  "Go To"
#define PROMPT_FIND  "Find"
#define PROMPT_SLASH "/"
#define PROMPT_UA    "Set User-Agent"

/* Set the property of surf using the old value. */
#define SETPROP(p, q, prompt) { \
	.v = (char *[]){ "/bin/sh", "-c", \
		"prop=\"`xprop -id $2 $0 " \
		"| sed \"s/^$0([A-Z0-9_]*) = \\(\\\\\"\\?\\)\\(.*\\)\\1$/\\2/\" " \
		"| xargs -0 printf %b | dmenu -p \"$3\"`\" &&" \
		"xprop -id $2 -f $1 8u -set $1 \"$prop\"", \
		p, q, winid, prompt, NULL \
	} \
}

/*
 * Set the property of surf using the old value and values supplied from a
 * file.
 */
#define SETPROPFROMFILE(p, q, prompt, file) { \
	.v = (char *[]){ "/bin/bash", "-c", \
		"prop=\"`{ xprop -id $2 $0 " \
		"| sed \"s/^$0([0-9A-Z_]*) = \\(\\\\\"\\?\\)\\(.*\\)\\1$/\\2/\" " \
		"| xargs -0 printf %b; " \
		"  [ -e $(eval echo $4) ] && cat $(eval echo $4); } " \
		"| dmenu -p \"$3\"`\" &&" \
		"xprop -id $2 -f $1 8u -set $1 \"$prop\"", \
		p, q, winid, prompt, file, NULL \
	} \
}

/* DOWNLOAD(URI, referer) */
#define DOWNLOAD(d, r) { \
	.v = (char *[]){ "/bin/sh", "-c", \
		"/bin/sh -c \"download '$1' '$2' '$3' '$0'\"", \
		d, useragent, r, cookiefile, NULL \
	} \
}

/* PLUMB(URI) */
/* This called when some URI which does not begin with "about:",
 * "http://" or "https://" should be opened.
 */
#define PLUMB(u) {\
	.v = (char *[]){ "/bin/sh", "-c", \
		"plumb \"$0\"", u, NULL \
	} \
}

/* styles */
/*
 * The iteration will stop at the first match, beginning at the beginning of
 * the list.
 */
static SiteStyle styles[] = {
	/* regexp		file in $styledir */
	{ ".*",			"default.css" },
};

#define MODKEY GDK_CONTROL_MASK

/* hotkeys */
/*
 * If you use anything else but MODKEY and GDK_SHIFT_MASK, don't forget to
 * edit the CLEANMASK() macro.
 */
static Key keys[] = {
	/* modifier	            keyval      function    arg             Focus */
	{ MODKEY|GDK_SHIFT_MASK,GDK_r,      reload,     { .b = TRUE } },
	{ MODKEY,               GDK_r,      reload,     { .b = FALSE } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_p,      print,      { 0 } },

	{ MODKEY,               GDK_p,      clipboard,  { .b = TRUE } },
	{ MODKEY,               GDK_y,      clipboard,  { .b = FALSE } },

	{ MODKEY|GDK_SHIFT_MASK,GDK_j,      zoom,       { .i = -1 } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_k,      zoom,       { .i = +1 } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_q,      zoom,       { .i = 0  } },
	{ MODKEY,               GDK_minus,  zoom,       { .i = -1 } },
	{ MODKEY,               GDK_plus,   zoom,       { .i = +1 } },

	{ MODKEY|GDK_SHIFT_MASK,GDK_l,      toggleinsecurecontent, { 0 } },
	{ MODKEY,               GDK_l,      navigate,   { .i = +1 } },
	{ MODKEY,               GDK_h,      navigate,   { .i = -1 } },

	{ MODKEY,               GDK_j,      scroll_v,   { .i = +1 } },
	{ MODKEY,               GDK_k,      scroll_v,   { .i = -1 } },
	{ MODKEY,               GDK_b,      scroll_v,   { .i = -10000 } },
	{ MODKEY,               GDK_space,  scroll_v,   { .i = +10000 } },
	{ MODKEY,               GDK_i,      scroll_h,   { .i = +1 } },
	{ MODKEY,               GDK_u,      scroll_h,   { .i = -1 } },

	{ 0,                    GDK_F11,    fullscreen, { 0 } },
	{ 0,                    GDK_Escape, stop,       { 0 } },
	{ MODKEY,               GDK_o,      source,     { 0 } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_o,      inspector,  { 0 } },

	{ MODKEY,               GDK_g,      spawn,      SETPROP("_SURF_URI", "_SURF_GO", PROMPT_GOTO) },
	{ MODKEY,               GDK_f,      spawn,      SETPROP("_SURF_FIND", "_SURF_FIND", PROMPT_FIND) },
	{ MODKEY,               GDK_slash,  spawn,      SETPROP("_SURF_FIND", "_SURF_FIND", PROMPT_SLASH) },
	{ MODKEY,               GDK_a,      spawn,      SETPROPFROMFILE("_SURF_UA", "_SURF_UA", PROMPT_UA, uafile) },

	{ MODKEY,               GDK_n,      find,       { .b = TRUE } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_n,      find,       { .b = FALSE } },

	{ MODKEY|GDK_SHIFT_MASK,GDK_c,      toggle,     { .v = "enable-caret-browsing" } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_i,      toggle,     { .v = "auto-load-images" } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_s,      toggle,     { .v = "enable-scripts" } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_v,      toggle,     { .v = "enable-plugins" } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_t,      togglesoup, { .v = "ssl-strict" } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_w,      toggle,     { .v = "enable-private-browsing" } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_a,      togglecookiepolicy, { 0 } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_m,      togglestyle, { 0 } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_b,      togglescrollbars, { 0 } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_g,      togglegeolocation, { 0 } },
	{ MODKEY|GDK_SHIFT_MASK,GDK_y,      toggleproxy, { 0 } },
};

/* button definitions */
/* click can be ClkDoc, ClkLink, ClkImg, ClkMedia, ClkSel, ClkEdit, ClkAny */
static Button buttons[] = {
	/* click                event mask  button  function        argument */
	{ ClkLink,              0,          2,      linkopenembed,  { 0 } },
	{ ClkLink,              MODKEY,     2,      linkopen,       { 0 } },
	{ ClkLink,              MODKEY,     1,      linkopen,       { 0 } },
	{ ClkAny,               0,          8,      navigate,       { .i = -1 } },
	{ ClkAny,               0,          9,      navigate,       { .i = +1 } },
};

